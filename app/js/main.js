///////////////////////////
// CUSTOM JS CODES
///////////////////////////

jQuery(document).ready(function ($) {

    //Menu smoothscroll and active menu
    $(document).on("scroll", onScroll);
    $('.main-header li  a[href^="#"]').on('click', function (e) {
        e.preventDefault();
        $(document).off("scroll");

        $('.main-header li').each(function () {
            $(this).removeClass('active');
        })
        $(this).parent().addClass('active');

        $target = $(this).attr('href');
        $('html, body').stop().animate({
            'scrollTop': $($target).offset().top - 71
        }, 1000, 'swing');
    });
    function onScroll(event){
        var scrollPos = $(document).scrollTop();
        $('.main-header li a').each(function () {
            var currLink = $(this);
            var refElement = $(currLink.attr("href") );
            if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() > scrollPos) {
                $('.main-header li').removeClass("active");
                currLink.parent().addClass("active");
            }
            else{
                currLink.parent().removeClass("active");
            }
        });
    }

});